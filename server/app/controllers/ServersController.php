<?php

class ServersController extends BaseController {

    public function getServers()
    {
		$ret = array();
		foreach(Server::all() AS &$server) {
			$instances = Instance::select('name', 'type as value')->where('server_id', $server->id)->get();
			$server['instances'] = $instances;
			$ret[] = $server;
		}
		return json_encode($ret);
    }

}