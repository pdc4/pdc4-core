<?php

class SegmentsController extends BaseController {

	public function getTypes($instance_id) {
		$status = Segment::join('TABLESPACES', 'TABLESPACES.id', '=', 'SEGMENTS.tablespace_id')
					->join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('count(SEGMENTS.id) as value, SEGMENTS.type as name'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('SEGMENTS.type')
					->orderBy('value', 'desc')
					->get();
		return json_encode($status);
	}
}