<?php

class InstancesController extends BaseController {

	public function getInstances() {
		return json_encode(Instance::all());
	}
	
    public function getTop($tri, $nb) {	
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = User::join('INSTANCES', 'USERS.instance_id', '=', 'INSTANCES.id')
					->select(DB::raw('count(USERS.id) as value, INSTANCES.name'))
					->groupBy('USERS.instance_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
    }
	
    public function getAvg() {
		$sub = Instance::select(DB::raw('count(USERS.id) as nb'))
					->join('USERS', 'USERS.instance_id', '=', 'INSTANCES.id')
					->groupBy('USERS.instance_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))->avg("a.nb"));
		return json_encode($avg);
    }

    public function getDatabases($id) {
		$databases = Database::select('name')
					->where('instance_id', $id)
					->get();
		return json_encode($databases);
    }
	
    public function getTopSize($tri, $nb) {	
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$sub = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('SUM(TABLESPACES.bytes) as value, DATABASES.instance_id as id'))
					->groupBy('TABLESPACES.database_id')
					->toSql();
		$top = DB::table( DB::raw("(" . $sub . ") as a"))
					->join('INSTANCES', 'INSTANCES.id', '=', 'a.id')
					->select(DB::raw('SUM(a.value) as value, INSTANCES.name'))
					->groupBy('a.id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
    }
	
    public function getAvgSize() {	
		$subsub = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('SUM(TABLESPACES.bytes) as value, DATABASES.instance_id as id'))
					->groupBy('TABLESPACES.database_id')
					->toSql();
		$sub = DB::table( DB::raw("(" . $subsub . ") as a"))
					->join('INSTANCES', 'INSTANCES.id', '=', 'a.id')
					->select(DB::raw('SUM(a.value) as nb'))
					->groupBy('a.id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))->avg("a.nb"));
		return json_encode($avg);
    }
	
}