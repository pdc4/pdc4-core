<?php

class UsersController extends BaseController {

	public function getStatus($instance_id) {
		$status = User::select(DB::raw('count(id) as value, status'))
					->where('instance_id', $instance_id)
					->groupBy('status')
					->orderBy('value', 'desc')
					->get();
		return json_encode($status);
	}

    public function getTopTables($instance_id, $tri, $nb) {	
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('count(TABLES.id) as value, USERS.name'))
					->where('USERS.instance_id', $instance_id)
					->groupBy('TABLES.user_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
    }
	
    public function getAvgTables($instance_id) {
		$sub = Table::join('USERS', 'USERS.id', '=', 'TABLES.user_id')
					->select(DB::raw('count(TABLES.id) as nb, USERS.instance_id as instance_id'))
					->groupBy('TABLES.user_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))
					->where('a.instance_id', $instance_id)
					->avg("a.nb"));
		return json_encode($avg);
    }
}