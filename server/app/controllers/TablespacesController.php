<?php

class TablespacesController extends BaseController {

	public function getTopSegments($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Segment::join('TABLESPACES', 'TABLESPACES.id', '=', 'SEGMENTS.tablespace_id')
					->join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('count(SEGMENTS.id) as value, TABLESPACES.name'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('SEGMENTS.tablespace_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}

	public function getAvgSegments($instance_id) {
		$sub = Segment::join('TABLESPACES', 'TABLESPACES.id', '=', 'SEGMENTS.tablespace_id')
					->join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('count(SEGMENTS.id) as nb, DATABASES.instance_id as instance_id'))
					->groupBy('SEGMENTS.tablespace_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))
					->where('a.instance_id', $instance_id)
					->avg("a.nb"));
		return json_encode($avg);
	}
	
	public function getTopSize($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('TABLESPACES.bytes as value, DATABASES.name as "database", TABLESPACES.name as name'))
					->where('DATABASES.instance_id', $instance_id)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgSize($instance_id) {
		$sub = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('TABLESPACES.bytes as nb, DATABASES.instance_id as instance_id'))
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))
					->where('a.instance_id', $instance_id)
					->avg("a.nb"));
		return json_encode($avg);
	}
}