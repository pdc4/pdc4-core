<?php

class TablesController extends BaseController {

	public function getTopValues($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.name, TABLES.nbValues as value, USERS.name as owner'))
					->where('USERS.instance_id', $instance_id)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgValues($instance_id) {
		$avg = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.nbValues'))
					->where('USERS.instance_id', $instance_id)
					->avg('TABLES.nbValues');
		return json_encode(array("value" => $avg));
	}

	public function getTopNullValues($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.name, TABLES.nbNullValues as value, USERS.name as owner'))
					->where('USERS.instance_id', $instance_id)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}

	public function getAvgNullValues($instance_id) {
		$avg = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.nbNullValues'))
					->where('USERS.instance_id', $instance_id)
					->avg('TABLES.nbNullValues');
		return json_encode(array("value" => $avg));
	}
	
	public function getTopAttributes($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.name, TABLES.nbAttributes as value, USERS.name as owner'))
					->where('USERS.instance_id', $instance_id)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgAttributes($instance_id) {
		$avg = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.nbAttributes'))
					->where('USERS.instance_id', $instance_id)
					->avg('TABLES.nbAttributes');
		return json_encode(array("value" => $avg));
	}
	
	public function getNilValues($instance_id) {
		$nb = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('count(TABLES.id) as value'))
					->where('USERS.instance_id', $instance_id)
					->where('TABLES.nbValues', 0)
					->get();
		return json_encode($nb[0]);
	}
	
	public function getTopIndex($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.name as name, TABLES.nbIndex as value, USERS.name as owner'))
					->where('USERS.instance_id', $instance_id)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgIndex($instance_id) {
		$avg = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.nbIndex'))
					->where('USERS.instance_id', $instance_id)
					->avg('TABLES.nbIndex');
		return json_encode(array("value" => $avg));
	}
	
	public function getTopNoindex($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$nb = Table::join('USERS', 'TABLES.user_id', '=', 'USERS.id')
					->select(DB::raw('TABLES.name as name, TABLES.nbValues as value, USERS.name as owner'))
					->where('USERS.instance_id', $instance_id)
					->where('TABLES.nbIndex', 0)
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($nb);
	}
}