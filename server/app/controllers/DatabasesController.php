<?php

class DatabasesController extends BaseController {

	public function getTopSize($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('SUM(TABLESPACES.bytes) as value, DATABASES.name'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('TABLESPACES.database_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgSize($instance_id) {
		$sub = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('SUM(TABLESPACES.bytes) as nb, DATABASES.instance_id as instance_id'))
					->groupBy('TABLESPACES.database_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))
					->where('a.instance_id', $instance_id)
					->avg("a.nb"));
		return json_encode($avg);
	}
	
	public function getTopFiles($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Tablespace::select(DB::raw('count(TABLESPACES.id) as value, DATABASES.name'))
					->groupBy('TABLESPACES.database_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		//return json_encode($top);
	}
	
	public function getAvgFiles($instance_id) {
		$sub = Tablespace::select(DB::raw('COUNT(TABLESPACES.id) as nb'))
					->groupBy('TABLESPACES.database_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))->avg("a.nb"));
		//return json_encode($avg);
	}
	
	public function getTopTablespaces($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('count(TABLESPACES.id) as value, DATABASES.name'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('TABLESPACES.database_id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgTablespace($instance_id) {
		$sub = Tablespace::join('DATABASES', 'DATABASES.id', '=', 'TABLESPACES.database_id')
					->select(DB::raw('count(TABLESPACES.id) as nb, DATABASES.instance_id as instance_id'))
					->groupBy('TABLESPACES.database_id')
					->toSql();
		$avg = array("value" => DB::table( DB::raw("(" . $sub . ") as a"))
					->where('a.instance_id', $instance_id)
					->avg("a.nb"));
		return json_encode($avg);
	}
	
	public function getTopRatio($instance_id, $tri, $nb) {
		$order = ($tri == 'top') ? 'desc' : 'asc';
		$top = Database::join('TABLES', 'TABLES.database_id', '=', 'DATABASES.id')
					->select(DB::raw('DATABASES.nbViews / COUNT(TABLES.id) as value, DATABASES.name as name'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('DATABASES.id')
					->orderBy('value', $order)
					->skip(0)->take($nb)
					->get();
		return json_encode($top);
	}
	
	public function getAvgRatio($instance_id) {
		$avgs = Database::join('TABLES', 'TABLES.database_id', '=', 'DATABASES.id')
					->select(DB::raw('DATABASES.nbViews as views, COUNT(TABLES.id) as tables'))
					->where('DATABASES.instance_id', $instance_id)
					->groupBy('DATABASES.id')
					->get();
		$array = array();
		foreach($avgs as $obj) {
			$array[] = $obj->views / $obj->tables;
		}
		return json_encode(array('value' => array_sum($array) / count($array)));
	}
	
}