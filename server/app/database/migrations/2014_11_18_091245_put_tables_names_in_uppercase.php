<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PutTablesNamesInUppercase extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$array = array('instances','servers','databases','tablespaces', 'files', 'users','tables');

		foreach ($array as $value) {
			Schema::rename($value, strtoupper($value));
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$array = array('INSTANCES','SERVERS','DATABASES','TABLESPACES', 'FILES', 'USERS','TABLES');

		foreach ($array as $key => $value) {
			Schema::rename($value, strtolower($value));
		}
	}

}
