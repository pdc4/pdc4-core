<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTablespacesSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('TABLESPACES', function($table)
		{
    		$table->string('name');
    		$table->string('type');
    		$table->string('status');
    		$table->string('fileName')->nullable();
    		$table->integer('bytes')->unsigned()->nullable();
    		$table->integer('blocks')->unsigned()->nullable();
    		
		});
		Schema::dropIfExists('FILES');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('TABLESPACES', function($table)
		{
    		$table->dropColumn('name','type','status','fileName','bytes','blocks');
		});

		Schema::create('FILES', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->float('size');
			$table->integer('tablespace_id')->length(10)->unsigned();
		});
		
		Schema::table('file', function($table) {
			$table->foreign('tablespace_id')
				  ->references('id')->on('tablespace')
				  ->onDelete('cascade');
		});
	}

}
