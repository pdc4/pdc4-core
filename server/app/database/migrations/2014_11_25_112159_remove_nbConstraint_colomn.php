<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNbConstraintColomn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('TABLES', function($table)
		{
    		$table->dropColumn('nbConstraints');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('TABLES', function($table)
		{
			$table->integer('nbConstraints');
		});
	}

}
