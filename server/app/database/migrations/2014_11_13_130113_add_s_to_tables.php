<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSToTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$array = array('instance','server','database','tablespace', 'file', 'user','table');

		foreach ($array as $value) {
			Schema::rename($value, $value.'s');
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$array = array('instance','server','database','tablespace', 'file', 'user','table');

		foreach ($array as $key => $value) {
			Schema::rename($value.'s', $value);
		}
	}

}
