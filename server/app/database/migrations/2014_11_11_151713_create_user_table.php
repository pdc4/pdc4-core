<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('instance_id')->length(10)->unsigned();
		});
		
		Schema::table('user', function($table) {
			$table->foreign('instance_id')
				  ->references('id')->on('instance');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user', function($table) {
			$table->dropForeign('user_tablespace_id_foreign');
		});
		Schema::drop('user');
	}

}
