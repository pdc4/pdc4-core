<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSegmentSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('SEGMENTS', function($table)
		{
			$table->increments('id');
    		$table->string('name');
    		$table->string('type');
    		$table->string('subtype');
    		$table->integer('bytes')->nullable();
    		$table->integer('blocks')->unsigned()->nullable();
    		$table->integer('tablespace_id')->length(10)->unsigned();
    		$table->integer('user_id')->length(10)->unsigned();
		});
		Schema::table('SEGMENTS', function($table) {
			$table->foreign('tablespace_id')
				  ->references('id')->on('TABLESPACES')
				  ->onDelete('cascade');
  			$table->foreign('user_id')
				  ->references('id')->on('USERS')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::table('SEGMENTS', function($table) {
			$table->dropForeign('SEGMENTS_tablespace_id_foreign');
			$table->dropForeign('SEGMENTS_user_id_foreign');
		});
		Schema::drop('SEGMENTS');
	}

}
