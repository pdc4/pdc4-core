<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `TABLES` MODIFY `nbAttributes` INTEGER  NULL;');
    	DB::statement('UPDATE `TABLES` SET `nbAttributes` = NULL WHERE `nbAttributes` = 0;');	
		DB::statement('ALTER TABLE `TABLES` MODIFY `nbValues` INTEGER  NULL;');
    	DB::statement('UPDATE `TABLES` SET `nbValues` = NULL WHERE `nbValues` = 0;');
		DB::statement('ALTER TABLE `TABLES` MODIFY `nbNullValues` INTEGER  NULL;');
    	DB::statement('UPDATE `TABLES` SET `nbNullValues` = NULL WHERE `nbConstraints` = 0;');
		DB::statement('ALTER TABLE `TABLES` MODIFY `nbConstraints` INTEGER  NULL;');
    	DB::statement('UPDATE `TABLES` SET `nbConstraints` = NULL WHERE `nbConstraints` = 0;');	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('UPDATE `TABLES` SET `nbAttributes` = 0 WHERE `nbAttributes` IS NULL;');
    	DB::statement('ALTER TABLE `TABLES` MODIFY `nbAttributes` INTEGER  NOT NULL;');
		DB::statement('UPDATE `TABLES` SET `nbValues` = 0 WHERE `nbValues` IS NULL;');
    	DB::statement('ALTER TABLE `TABLES` MODIFY `nbValues` INTEGER  NOT NULL;');
		DB::statement('UPDATE `TABLES` SET `nbNullValues` = 0 WHERE `nbNullValues` IS NULL;');
    	DB::statement('ALTER TABLE `TABLES` MODIFY `nbNullValues` INTEGER  NOT NULL;');
		DB::statement('UPDATE `TABLES` SET `nbConstraints` = 0 WHERE `nbConstraints` IS NULL;');
    	DB::statement('ALTER TABLE `TABLES` MODIFY `nbConstraints` INTEGER  NOT NULL;');
	}

}
