<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->float('size');
			$table->integer('tablespace_id')->length(10)->unsigned();
		});
		
		Schema::table('file', function($table) {
			$table->foreign('tablespace_id')
				  ->references('id')->on('tablespace')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('file', function($table) {
			$table->dropForeign('file_tablespace_id_foreign');
		});
		Schema::drop('file');
	}

}
