<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('database', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('instance_id')->length(10)->unsigned();
		});
		
		Schema::table('database', function($table) {
			$table->foreign('instance_id')
				  ->references('id')->on('instance')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('database', function($table) {
			$table->dropForeign('database_instance_id_foreign');
		});
		Schema::drop('database');
	}

}
