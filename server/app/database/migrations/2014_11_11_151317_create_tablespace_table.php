<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablespaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tablespace', function($table)
		{
			$table->increments('id');
			$table->integer('database_id')->length(10)->unsigned();
		});
		
		Schema::table('tablespace', function($table) {
			$table->foreign('database_id')
				  ->references('id')->on('database')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tablespace', function($table) {
			$table->dropForeign('tablespace_database_id_foreign');
		});
		Schema::drop('tablespace');
	}

}
