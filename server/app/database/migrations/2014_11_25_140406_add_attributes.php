<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('DATABASES', function($table)
		{
			$table->integer('nbViews');
		});
		Schema::table('SERVERS', function($table)
		{
			$table->string('os');
			$table->string('osVersion');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('DATABASES', function($table)
		{
    		$table->dropColumn('nbViews');
		});
		Schema::table('SERVERS', function($table)
		{
    		$table->dropColumn('os');
    		$table->dropColumn('osVersion');
		});
	}

}
