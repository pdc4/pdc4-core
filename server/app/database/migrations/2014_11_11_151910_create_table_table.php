<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('table', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('nbAttributes');
			$table->integer('nbValues');
			$table->integer('nbNullValues');
			$table->integer('nbConstraints');
			$table->integer('database_id')->length(10)->unsigned();
			$table->integer('user_id')->length(10)->unsigned();
		});
		
		Schema::table('table', function($table) {
			$table->foreign('database_id')
				  ->references('id')->on('database')
				  ->onDelete('cascade');
		});
		
		Schema::table('table', function($table) {
			$table->foreign('user_id')
				  ->references('id')->on('user')
				  ->onDelete('cascade');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('table', function($table) {
			$table->dropForeign('table_database_id_foreign');
		});
		Schema::table('table', function($table) {
			$table->dropForeign('table_user_id_foreign');
		});
		Schema::drop('table');
	}

}
