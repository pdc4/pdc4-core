<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('server', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('ram');
			$table->integer('storage');
		});
		
		Schema::table('instance', function($table) {
			$table->foreign('server_id')
				  ->references('id')->on('server')
				  ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('instance', function($table) {
			$table->dropForeign('instance_server_id_foreign');
		});
		Schema::drop('server');
	}

}
