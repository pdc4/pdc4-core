<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNbIndexNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `TABLES` MODIFY `nbIndex` INTEGER  NULL;');
    	DB::statement('UPDATE `TABLES` SET `nbIndex` = NULL WHERE `nbIndex` = 0;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('UPDATE `TABLES` SET `nbIndex` = 0 WHERE `nbIndex` IS NULL;');
    	DB::statement('ALTER TABLE `TABLES` MODIFY `nbIndex` INTEGER  NOT NULL;');
	}

}
