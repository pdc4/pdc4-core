<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/**** Servers ******************************************************************/
Route::get('servers', 'ServersController@getServers');

/**** Instances ******************************************************************/
Route::get('instances', 'InstancesController@getInstances');
Route::get('instances/nbUsers/{tri}/{id}', 'InstancesController@getTop');
Route::get('instances/nbUsers/avg', 'InstancesController@getAvg');
Route::get('instances/{id}/databases', 'InstancesController@getDatabases');
Route::get('instances/size/{tri}/{id}', 'InstancesController@getTopSize');
Route::get('instances/size/avg', 'InstancesController@getAvgSize');

/**** Users ******************************************************************/
Route::get('{instanceId}/users/nbTables/{tri}/{id}', 'UsersController@getTopTables');
Route::get('{instanceId}/users/nbTables/avg', 'UsersController@getAvgTables');
Route::get('{instanceId}/users/status', 'UsersController@getStatus');

/**** Tables ******************************************************************/
Route::get('{instanceId}/tables/nbValues/{tri}/{id}', 'TablesController@getTopValues');
Route::get('{instanceId}/tables/nbValues/avg', 'TablesController@getAvgValues');
Route::get('{instanceId}/tables/nbNullValues/{tri}/{id}', 'TablesController@getTopNullValues');
Route::get('{instanceId}/tables/nbNullValues/avg', 'TablesController@getAvgNullValues');
Route::get('{instanceId}/tables/nbAttributes/{tri}/{id}', 'TablesController@getTopAttributes');
Route::get('{instanceId}/tables/nbAttributes/avg', 'TablesController@getAvgAttributes');
Route::get('{instanceId}/tables/nbNilValues', 'TablesController@getNilValues');
Route::get('{instanceId}/tables/nbIndex/{tri}/{nb}', 'TablesController@getTopIndex');
Route::get('{instanceId}/tables/nbIndex/avg', 'TablesController@getAvgIndex');
Route::get('{instanceId}/tables/noIndex/{tri}/{nb}', 'TablesController@getTopNoindex');

/**** Databases ******************************************************************/
Route::get('{instanceId}/databases/size/{tri}/{id}', 'DatabasesController@getTopSize');
Route::get('{instanceId}/databases/size/avg', 'DatabasesController@getAvgSize');
Route::get('{instanceId}/databases/nbFiles/{tri}/{id}', 'DatabasesController@getTopFiles');
Route::get('{instanceId}/databases/nbFiles/avg', 'DatabasesController@getAvgFiles');
Route::get('{instanceId}/databases/nbTablespaces/{tri}/{id}', 'DatabasesController@getTopTablespaces');
Route::get('{instanceId}/databases/nbTablespaces/avg', 'DatabasesController@getAvgTablespace');
Route::get('{instanceId}/databases/ratioView/{tri}/{nb}', 'DatabasesController@getTopRatio');
Route::get('{instanceId}/databases/ratioView/avg', 'DatabasesController@getAvgRatio');

/**** Tablespace ****************************************************************/
Route::get('{instanceId}/tablespaces/nbSegments/{tri}/{nb}', 'TablespacesController@getTopSegments');
Route::get('{instanceId}/tablespaces/nbSegments/avg', 'TablespacesController@getAvgSegments');
Route::get('{instanceId}/tablespaces/size/{tri}/{nb}', 'TablespacesController@getTopSize');
Route::get('{instanceId}/tablespaces/size/avg', 'TablespacesController@getAvgSize');

/**** Segments ******************************************************************/
Route::get('{instanceId}/segments/types', 'SegmentsController@getTypes');
