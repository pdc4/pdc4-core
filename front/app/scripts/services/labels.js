'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.indicators
 * @description
 * # indicators
 * Constant in the dashboardApp
.
 */
angular.module('dashboardApp')
    .constant('labels', {
        cartography: 'Cartographie',
        generalCartography: 'Cartographie Générale',
        physicLogic: 'Rapport physique / logique',
        dataQuality: 'Qualité des données',
        users: 'Utilisateurs',
        databases: 'Bases de données',
        instances: 'Instances',
        segments: 'Segments',
        servers: 'Serveurs',
        tablespaces: 'Tablespaces',
        tables: 'Tables',
        index: 'Index'
    });
