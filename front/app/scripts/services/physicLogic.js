'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.examples
 * @description
 * # examples
 * Constant in the dashboardApp
.
 */
angular.module('dashboardApp')
    .constant('physicLogic', [
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Ratios par base de données : nombre de vues divisé par le nombre de tables',
            section: 'databases',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Ratio',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} ratios'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'databases',
                indicator: 'ratioView',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseFloat(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'databases',
                    indicator: 'ratioView',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(parseFloat(response.value), 2);
                },
                text: 'Ratio moyen :'
            }],
            width: 6,
            order: 2
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre de tablespace par base de données',
            section: 'databases',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de tablespaces',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres de tablespaces par base de données'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'databases',
                indicator: 'nbTablespaces',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'databases',
                    indicator: 'nbTablespaces',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(response.value, 2);
                },
                text: 'Nombre moyen par base de données :'
            }],
            width: 6,
            order: 1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Taille des bases de données',
            section: 'databases',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Taille de la base de données (en Octets)',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} bases de données'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'databases',
                indicator: 'size',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'databases',
                    indicator: 'size',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('byteFmt')(parseInt(response.value), 2);
                },
                text: 'Taille moyenne :'
            }],
            width: 6,
            order: 3
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre d\'index par table',
            section: 'index',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre d\'index',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres d\'index par table'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tables',
                indicator: 'nbIndex',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.owner + '.' + value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tables',
                    indicator: 'nbIndex',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(response.value, 2);
                },
                text: 'Nombre moyen par tables :'
            }],
            width: 6,
            order: 1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Tables avec 0 index, triées par nombre de tuples',
            section: 'index',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de tuples',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} tables ayant 0 index'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tables',
                indicator: 'noIndex',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.owner + '.' + value.name);
                });
            },
            width: 6,
            order: 2
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Répartition des types de segment',
            section: 'segments',
            chartConfig: {
                options: {
                    chart: {
                        renderTo: 'container',
                        type: 'pie'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        },
                        series: {
                            dataLabels: {
                                enabled: true,
                                formatter: function() {
                                    return Math.round(this.percentage * 100) / 100 + ' %';
                                },
                                distance: -60,
                                color: 'white'
                            }
                        }
                    }
                },
                title: {
                    text: 'Répartition des types de segment'
                },
                series: [{
                    type: 'pie',
                    name: 'Nombre',
                    data: []

                }]
            },
            routeParams: {
                table: 'segments',
                indicator: 'types'
            },
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push([value.name, parseInt(value.value)]);
                });
            },
            width: 6,
            order: 1.2
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Taille des tablespaces',
            section: 'tablespaces',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Taille du tablespace',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} tablespaces'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tablespaces',
                indicator: 'size',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.database + '.' + value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tablespaces',
                    indicator: 'size',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('byteFmt')(parseInt(response.value), 2);
                },
                text: 'Taille moyenne :'
            }],
            width: 6,
            order: 1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre de segments par tablespace',
            section: 'tablespaces',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de segments',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres de segments par tablespace'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tablespaces',
                indicator: 'nbSegments',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tablespaces',
                    indicator: 'nbSegments',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(response.value, 2);
                },
                text: 'Nombre moyen par tablespace :'
            }],
            width: 6,
            order: 2
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ]);
