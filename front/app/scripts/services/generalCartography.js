'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.indicators
 * @description
 * # indicators
 * Constant in the dashboardApp
.
 */
angular.module('dashboardApp')
    .constant('generalCartography', [{
            title: 'Liste des serveurs',
            section: 'servers',
            table: {
                head: [{
                    key: 'name',
                    text: 'Nom',
                    filter: {
                        name: 'ucfirst'
                    },
                    width: 2,
                    order: 1
                }, {
                    key: 'instances',
                    text: 'Nombre d\'instances',
                    width: 2,
                    order: 2
                }, {
                    key: 'ram',
                    text: 'RAM',
                    filter: {
                        name: 'byteFmt',
                        parameter: 2
                    },
                    width: 1,
                    order: 3
                }, {
                    key: 'storage',
                    text: 'Stockage',
                    filter: {
                        name: 'byteFmt',
                        parameter: 2
                    },
                    width: 1,
                    order: 4
                }, {
                    key: 'os',
                    text: 'OS',
                    width: 2,
                    order: 5
                }, {
                    key: 'osVersion',
                    text: 'Version de l\'OS',
                    width: 2,
                    order: 6
                }]
            },
            routeParams: {
                table: 'servers'
            },
            APIfunction: 'getArray',
            build: function(instance, response) {
                angular.forEach(response, function(row) {
                    row.ram = parseInt(row.ram);
                    row.storage = parseInt(row.storage);
                });
                instance.table.body = response;
            },
            width: 12,
            order: 1.1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Répartition des SGBD par type',
            section: 'instances',
            chartConfig: {
                options: {
                    chart: {
                        renderTo: 'container',
                        type: 'pie'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        },
                        series: {
                            dataLabels: {
                                enabled: true,
                                formatter: function() {
                                    return Math.round(this.percentage * 100) / 100 + ' %';
                                },
                                distance: -60,
                                color: 'white'
                            }
                        }
                    }
                },
                title: {
                    text: 'Répartition des SGBD par type'
                },
                series: [{
                    type: 'pie',
                    name: 'Nombre',
                    data: []

                }]
            },
            routeParams: {
                table: 'instances'
            },
            APIfunction: 'getArray',
            build: function(instance, response, $filter) {
                var groupedByType = $filter('groupBy')(response, 'type');
                instance.chartConfig.series[0].data = [];
                angular.forEach(groupedByType, function(array, type) {
                    instance.chartConfig.series[0].data.push([$filter('ucfirst')(type), array.length]);
                });
            },
            width: 6,
            order: 1.2
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre d\'utilisateurs par instance',
            section: 'instances',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre d\'utilisateurs',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres d\'utilisateurs par instance'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'instances',
                indicator: 'nbUsers',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArray',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'instances',
                    indicator: 'nbUsers',
                    type: 'avg'
                },
                APIfunction: 'get',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(response.value, 2);
                },
                text: 'Nombre moyen d\'utilisateurs par instance :'
            }],
            width: 6,
            order: 2.1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Taille des instances',
            section: 'instances',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Taille de l\'instance',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} instances'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'instances',
                indicator: 'size',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArray',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'instances',
                    indicator: 'size',
                    type: 'avg'
                },
                APIfunction: 'get',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('byteFmt')(parseInt(response.value), 2);
                },
                text: 'Taille moyenne des instances :'
            }],
            width: 12,
            order: 2.2
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ]);
