'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.indicators
 * @description
 * # indicators
 * Constant in the dashboardApp
.
 */
angular.module('dashboardApp')
    .constant('dataQuality', [{
            title: 'Nombre de valeurs nulles par table',
            section: 'tables',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de valeurs nulles',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres de valeurs nulles par table'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tables',
                indicator: 'nbNullValues',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.owner + '.' + value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tables',
                    indicator: 'nbNullValues',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response) {
                    indicator.value = response.value;
                },
                text: 'Nombre moyen par table :'
            }],
            width: 6,
            order: 1.1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre de tuples par table',
            section: 'tables',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de tuples',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres de tuples par table'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tables',
                indicator: 'nbValues',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.owner + '.' + value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tables',
                    indicator: 'nbValues',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(parseFloat(response.value), 2);

                },
                text: 'Nombre moyen de tuples :'
            }, {
                routeParams: {
                    table: 'tables',
                    indicator: 'nbNilValues'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(parseInt(response.value));
                },
                text: 'Nombre de tables avec 0 tuple :'
            }],
            width: 6,
            order: 2.1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre d\'attributs par table',
            section: 'tables',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre d\'attributs',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres d\'attributs par table'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'tables',
                indicator: 'nbAttributes',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.owner + '.' + value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'tables',
                    indicator: 'nbAttributes',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(parseFloat(response.value), 2);
                },
                text: 'Nombre d\'attributs moyen :'
            }],
            width: 6,
            order: 2.3
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ]);
