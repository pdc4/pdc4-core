'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.indicators
 * @description
 * # indicators
 * Constant in the dashboardApp
.
 */
angular.module('dashboardApp')
    .constant('cartography', [
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Nombre de tables par utilisateur',
            section: 'users',
            chartConfig: {
                options: {
                    chart: {
                        type: 'bar'
                    }
                },
                series: [{
                    name: 'Nombre de tables',
                    data: []
                }],
                title: {
                    originalText: 'Top {{number}} des plus {{type}} nombres de tables par utilisateur'
                },
                xAxis: {
                    categories: []
                },
                loading: false
            },
            routeParams: {
                table: 'users',
                indicator: 'nbTables',
                type: 'top',
                nb: '5'
            },
            select: [{
                value: '5',
                label: '5'
            }, {
                value: '10',
                label: '10'
            }, {
                value: '20',
                label: '20'
            }],
            selectTopBottom: [{
                value: 'top',
                label: 'top'
            }, {
                value: 'bottom',
                label: 'bottom'
            }],
            APIfunction: 'getArrayWithInstance',
            build: function(indicator, response) {
                indicator.chartConfig.series[0].data = [];
                indicator.chartConfig.xAxis.categories = [];
                response.forEach(function(value) {
                    indicator.chartConfig.series[0].data.push(parseInt(value.value));
                    indicator.chartConfig.xAxis.categories.push(value.name);
                });
            },
            notes: [{
                routeParams: {
                    table: 'users',
                    indicator: 'nbTables',
                    type: 'avg'
                },
                APIfunction: 'getWithInstance',
                build: function(indicator, response, $filter) {
                    indicator.value = $filter('number')(response.value, 2);
                },
                text: 'Nombre moyen par utilisateur :'
            }],
            width: 6,
            order: 3.1
        },
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            title: 'Répartition des utilisateurs par status',
            section: 'users',
            chartConfig: {
                options: {
                    chart: {
                        renderTo: 'container',
                        type: 'pie'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        },
                        series: {
                            dataLabels: {
                                enabled: true,
                                formatter: function() {
                                    return Math.round(this.percentage * 100) / 100 + ' %';
                                },
                                distance: -60,
                                color: 'white'
                            }
                        }
                    }
                },
                title: {
                    text: 'Répartition des utilisateurs par status'
                },
                series: [{
                    type: 'pie',
                    name: 'Nombre',
                    data: []

                }]
            },
            routeParams: {
                table: 'users',
                indicator: 'status'
            },
            APIfunction: 'getArrayWithInstance',
            build: function(instance, response, $filter) {
                instance.chartConfig.series[0].data = [];
                angular.forEach(response, function(type) {
                    instance.chartConfig.series[0].data.push([$filter('ucfirst')(type.status), parseInt(type.value)]);
                });
            },
            width: 6,
            order: 3.2
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ]);
