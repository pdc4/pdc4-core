'use strict';

/**
 * @ngdoc service
 * @name dashboardApp.api
 * @description
 * # api
 * Service in the dashboardApp.
 */
angular.module('dashboardApp')
    .service('api', function api($resource) {
        var apiRequester = $resource('/api/:instance/:table/:indicator/:type/:nb', {
            instance: '',
            table: '',
            indicator: '',
            type: '',
            nb: ''
        }, {
            get: {
                method: 'GET',
                isArray: false
            },
            getArray: {
                method: 'GET',
                isArray: true
            }
        });
        return {
            getWithInstance: function(routeParams) {
                return apiRequester.get(routeParams).$promise;
            },
            getArrayWithInstance: function(routeParams) {
                return apiRequester.getArray(routeParams).$promise;
            },
            get: function(routeParams) {
                routeParams.instance = '';
                return apiRequester.get(routeParams).$promise;
            },
            getArray: function(routeParams) {
                routeParams.instance = '';
                return apiRequester.getArray(routeParams).$promise;
            }
        };
    });
