'use strict';

/**
 * @ngdoc overview
 * @name dashboardApp
 * @description
 * # dashboardApp
 *
 * Main module of the application.
 */
angular
    .module('dashboardApp', [
        'ngResource',
        'ngRoute',
        'highcharts-ng',
        'ngSanitize',
        'mgcrea.ngStrap',
        'duScroll',
        'angular.filter'
    ])
    .value('duScrollOffset', 60)
    .config(function($tooltipProvider, $routeProvider) {
        angular.extend($tooltipProvider.defaults, {
            html: true
        });
        $routeProvider
            .when('/specific', {
                templateUrl: 'views/wall.html',
                controller: 'SpecificCtrl'
            })
            .when('/general', {
                templateUrl: 'views/wall.html',
                controller: 'GeneralCtrl'
            })
            .otherwise({
                redirectTo: '/general'
            });
    });
