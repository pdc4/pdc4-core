'use strict';

/**
 * @ngdoc directive
 * @name dashboardApp.directive:panel
 * @description
 * # panel
 */
angular.module('dashboardApp')
    .directive('panel', function() {
        return {
            templateUrl: 'views/panel.html',
            restrict: 'E',
            scope: {
                indicator: '='
            },
            controller: 'PanelCtrl'
        };
    });
