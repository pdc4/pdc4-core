'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:NoteCtrl
 * @description
 * # NoteCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('NoteCtrl', function($scope, api, $log, $filter, $rootScope) {


        function getValues() {
            var routeParams = angular.copy($scope.note.routeParams);
            routeParams.instance = $rootScope.instance.id;

            api[$scope.note.APIfunction](routeParams).then(function(res) {
                $log.info(res);
                $scope.note.build($scope.note, res, $filter);
            }, function() {
                $log.error('Error during the request for the note :', $scope.note.text);
            });
        }

        if ($scope.note.routeParams) {
            $rootScope.$watch('instance', function() {
                getValues();
            });
        }
    });
