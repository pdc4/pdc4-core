'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:GeneralCtrl
 * @description
 * # GeneralCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('GeneralCtrl', function($scope, generalCartography, labels) {
        $scope.array = {
            generalCartography: angular.copy(generalCartography)
        };
        $scope.labels = labels;
    });
