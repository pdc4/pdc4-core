'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:SelectinstanceCtrl
 * @description
 * # SelectinstanceCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('SelectInstanceCtrl', function($rootScope, $scope, api, $log, $location) {
        api.getArray({
            table: 'instances'
        }).then(function(res) {
            $log.info(res);
            $rootScope.instances = res;
            if (res.length > 0) {
                $rootScope.instance = res[0];
            }
        }, function() {
            $log.error('Error during the request for the list of indicator');
        });

        $scope.getLabel = function() {
            if ($location.path() === '/specific') {
                if ($rootScope.instance) {
                    return $rootScope.instance.name;
                } else {
                    return '';
                }
            } else {
                return 'Sélectionner une instance';
            }
        };

        $scope.select = function(instance) {
            $rootScope.instance = instance;
            $location.path('/specific');
        };

        $scope.selected = function(instance) {
            if ($location.path() === '/specific') {
                return $rootScope.instance === instance;
            } else {
                return false;
            }
        };
    });
