'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:SpecificCtrl
 * @description
 * # SpecificCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('SpecificCtrl', function($scope, dataQuality, physicLogic, cartography, labels) {
        $scope.array = {
            dataQuality: angular.copy(dataQuality),
            physicLogic: angular.copy(physicLogic),
            cartography: angular.copy(cartography)
        };
        $scope.labels = labels;
    });
