'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:RowCtrl
 * @description
 * # RowCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('RowCtrl', function($scope) {
        $scope.text = $scope.row[$scope.header.key].length;

        $scope.tooltip = {
            title: '',
            placement: 'bottom'
        };

        angular.forEach($scope.row[$scope.header.key], function(instance) {
            $scope.tooltip.title += instance.name + ' : ' + instance.value + '<br/>';
        });


    });
