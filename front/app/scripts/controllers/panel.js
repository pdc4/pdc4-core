'use strict';

/**
 * @ngdoc function
 * @name dashboardApp.controller:PanelCtrl
 * @description
 * # PanelCtrl
 * Controller of the dashboardApp
 */
angular.module('dashboardApp')
    .controller('PanelCtrl', function($scope, $filter, api, $log, $rootScope) {
        if ($scope.indicator.table) {
            $scope.orderProp = $filter('orderBy')($scope.indicator.table.head, 'order')[0].key;
            $scope.reverse = false;

            $scope.orderBy = function(prop, reverse) {
                $scope.orderProp = prop;
                $scope.reverse = reverse;
            };

            $scope.orderFunc = function(prop) {
                if (angular.isArray(prop[$scope.orderProp])) {
                    return prop[$scope.orderProp].length;
                } else {
                    return prop[$scope.orderProp];
                }
            };

            $scope.getValue = function(row, header) {
                if (header.filter) {
                    return $filter(header.filter.name)(row[header.key], header.filter.parameter);
                }
                return row[header.key];
            };
        }

        $scope.isArray = function(value) {
            return angular.isArray(value);
        };

        function getValues() {
            var routeParams = angular.copy($scope.indicator.routeParams);
            routeParams.instance = $rootScope.instance.id;

            api[$scope.indicator.APIfunction](routeParams).then(function(res) {
                $log.info(res);
                $scope.indicator.build($scope.indicator, res, $filter);
                if ($scope.indicator.chartConfig && $scope.indicator.chartConfig.title.originalText) {
                    var title = $scope.indicator.chartConfig.title;
                    var typeText = $scope.indicator.routeParams.type === 'top' ? 'grand(e)s' : 'petit(e)s';
                    title.text = title.originalText.replace('{{number}}', $scope.indicator.routeParams.nb)
                        .replace('{{type}}', typeText);
                }
            }, function() {
                $log.error('Error during the request for the indicator :', $scope.indicator.title);
            });
        }

        if ($scope.indicator.routeParams) {
            var group = [];
            if ($scope.indicator.select) {
                group.push('indicator.routeParams.nb');
            }
            if ($scope.indicator.selectTopBottom) {
                group.push('indicator.routeParams.type');
            }
            if (group.length > 0) {
                $scope.$watchGroup(group, function() {
                    getValues();
                });
            }

            $rootScope.$watch('instance', function() {
                getValues();
            });
        }

    });
