
--#Execute 
use homestead;
source ~/pdc4-core/queries/temp/servers.sql;
source ~/pdc4-core/queries/temp/instances.sql;
source ~/pdc4-core/queries/temp/databases.sql;

source ~/pdc4-core/queries/temp/IFusers.sql;
source ~/pdc4-core/queries/temp/IFtables.sql;
source ~/pdc4-core/queries/temp/IFvaluesUpdate.sql;
source ~/pdc4-core/queries/temp/IFtablespaces.sql;
source ~/pdc4-core/queries/temp/IFtablespacesFiles.sql;
source ~/pdc4-core/queries/temp/IFtablespacesTempFiles.sql;
source ~/pdc4-core/queries/temp/IFsegments.sql;
source ~/pdc4-core/queries/temp/IFviewCount.sql;
source ~/pdc4-core/queries/temp/IFnullValues.sql;
source ~/pdc4-core/queries/temp/IFnbIndex.sql;


source ~/pdc4-core/queries/temp/LIRISusers.sql;
source ~/pdc4-core/queries/temp/LIRIStables.sql;
source ~/pdc4-core/queries/temp/LIRISvaluesUpdate.sql;
source ~/pdc4-core/queries/temp/LIRIStablespaces.sql;
source ~/pdc4-core/queries/temp/LIRIStablespacesFiles.sql;
source ~/pdc4-core/queries/temp/LIRISsegments.sql;
source ~/pdc4-core/queries/temp/LIRISviewCount.sql;
source ~/pdc4-core/queries/temp/LIRISnullValues.sql;
source ~/pdc4-core/queries/temp/LIRISnbIndex.sql;
