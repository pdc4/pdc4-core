SET TERMOUT off;
SET verify off;
SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
SPOOL createValNull.sql;

select '@pdc4_if_null2.sql ' || '"' ||OWNER || '" "' || TABLE_NAME || '";'
from DBA_TABLES
where OWNER NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');

spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;