@pdc4_if_null2.sql "OUTLN" "OL$";                                                     
@pdc4_if_null2.sql "OUTLN" "OL$HINTS";                                                
@pdc4_if_null2.sql "OUTLN" "OL$NODES";                                                
@pdc4_if_null2.sql "DBSNMP" "MGMT_SNAPSHOT";                                          
@pdc4_if_null2.sql "DBSNMP" "MGMT_SNAPSHOT_SQL";                                      
@pdc4_if_null2.sql "DBSNMP" "MGMT_BASELINE";                                          
@pdc4_if_null2.sql "DBSNMP" "MGMT_BASELINE_SQL";                                      
@pdc4_if_null2.sql "DBSNMP" "MGMT_CAPTURE";                                           
@pdc4_if_null2.sql "DBSNMP" "MGMT_CAPTURE_SQL";                                       
@pdc4_if_null2.sql "DBSNMP" "MGMT_RESPONSE_CONFIG";                                   
@pdc4_if_null2.sql "DBSNMP" "MGMT_LATEST";                                            
@pdc4_if_null2.sql "DBSNMP" "MGMT_LATEST_SQL";                                        
@pdc4_if_null2.sql "DBSNMP" "MGMT_HISTORY";                                           
@pdc4_if_null2.sql "DBSNMP" "MGMT_HISTORY_SQL";                                       
@pdc4_if_null2.sql "DBSNMP" "BSLN_METRIC_DEFAULTS";                                   
@pdc4_if_null2.sql "DBSNMP" "BSLN_BASELINES";                                         
@pdc4_if_null2.sql "DBSNMP" "BSLN_STATISTICS";                                        
@pdc4_if_null2.sql "DBSNMP" "BSLN_THRESHOLD_PARAMS";                                  
@pdc4_if_null2.sql "DBSNMP" "MGMT_DB_FEATURE_LOG";                                    
@pdc4_if_null2.sql "APPQOSSYS" "WLM_METRICS_STREAM";                                  
@pdc4_if_null2.sql "APPQOSSYS" "WLM_CLASSIFIER_PLAN";                                 
@pdc4_if_null2.sql "ORDSYS" "ORD_CARTRIDGE_COMPONENTS";                               
@pdc4_if_null2.sql "ORDSYS" "ORD_USAGE_RECS";                                         
@pdc4_if_null2.sql "ORDSYS" "SI_IMAGE_FORMATS_TAB";                                   
@pdc4_if_null2.sql "ORDSYS" "SI_FEATURES_TAB";                                        
@pdc4_if_null2.sql "ORDSYS" "SI_VALUES_TAB";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM$HIERARCHY";                                         
@pdc4_if_null2.sql "OLAPSYS" "CWM$DIMENSIONATTRIBUTE";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM$FUNCTION";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM$FACTUSE";                                           
@pdc4_if_null2.sql "OLAPSYS" "CWM$DOMAIN";                                            
@pdc4_if_null2.sql "OLAPSYS" "CWM$ARGUMENT";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM$MEASURE";                                           
@pdc4_if_null2.sql "OLAPSYS" "CWM$CLASSIFICATIONTYPE";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM$PARAMETER";                                         
@pdc4_if_null2.sql "OLAPSYS" "CWM$CUBE";                                              
@pdc4_if_null2.sql "OLAPSYS" "CWM$LEVEL";                                             
@pdc4_if_null2.sql "OLAPSYS" "CWM$CUBEDIMENSIONUSE";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM$FUNCTIONUSE";                                       
@pdc4_if_null2.sql "OLAPSYS" "CWM$CLASSIFICATIONENTRY";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM$ITEMUSE";                                           
@pdc4_if_null2.sql "OLAPSYS" "CWM$PROJECT";                                           
@pdc4_if_null2.sql "OLAPSYS" "CWM$CLASSIFICATION";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM$FACTTABLEMAP";                                      
@pdc4_if_null2.sql "OLAPSYS" "CWM$MEASUREDIMENSIONUSE";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM$FACTLEVELGROUP";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM$MODEL";                                             
@pdc4_if_null2.sql "OLAPSYS" "CWM$LEVELATTRIBUTE";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM$FACTLEVELUSE";                                      
@pdc4_if_null2.sql "OLAPSYS" "CWM$DIMENSION";                                         
@pdc4_if_null2.sql "OLAPSYS" "CWM$OBJECTTYPE";                                        
@pdc4_if_null2.sql "OLAPSYS" "CWM$ITEMMAP";                                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$DIMENSION";                                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$LEVEL";                                            
@pdc4_if_null2.sql "OLAPSYS" "CWM2$HIERARCHY";                                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$HIERCUSTOMSORT";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$HIERLEVELREL";                                     
@pdc4_if_null2.sql "OLAPSYS" "CWM2$DIMENSIONATTRIBUTE";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM2$LEVELATTRIBUTE";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$LEVELATTRIBUTEMAP";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$CUBE";                                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MEASURE";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$CUBEDIMENSIONUSE";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$DIMHIERLVLMAP";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AW_DIMENSIONMAP";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM2$FACTDIMHIERMAP";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$FACTDIMHIERTPLSDTL";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MEASURETABLEMAP";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM2$FACTKEYDIMHIERMAP";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$FACTKEYDIMHIERLVLMAP";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AW_MEASUREMAP";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM2$STOREDDIMLVLTPLS";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$STOREDDIMLVLTPLSDTL";                              
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWVIEWS";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWVIEWCOLS";                                       
@pdc4_if_null2.sql "OLAPSYS" "CWM2$CLASSIFICATIONVALUEPAIR";                          
@pdc4_if_null2.sql "OLAPSYS" "XML_LOAD_RECORDS";                                      
@pdc4_if_null2.sql "OLAPSYS" "XML_LOAD_LOG";                                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$_AW_NEXT_PERM_CUST_MEAS";                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$_AW_PERM_CUST_MEAS_MAP";                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMLOAD";                                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMLOADFILTER";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMLOADPARM";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMLOADPARMVALUE";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMLOADTYPE";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOAD";                                       
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADFILTER";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADPARM";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADPARMVALUE";                              
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADTYPE";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADMEASURE";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBELOADAGGPLAN";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBEAGG";                                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBEAGGLEVEL";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBEAGGMEASURE";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCOMPOSITESPEC";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCOMPSPECMEMBERSHIP";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBECOMPPLAN";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_AWVIEWS";                                    
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_AWVIEWCOLS";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_CATALOGS";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_CATALOG_ENTITY_USES";                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_LISTDIMS";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_DIM_HIERS";                                  
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_HIERDIMS";                                   
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_DIM_HIER_LEVEL_USES";                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_JOIN_KEY_COL_USES";                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_DIM_ATTRIBUTES";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_ENTITY_DESC_USES";                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_DESCRIPTORS";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_DIM_LEVEL_ATTR_MAPS";                        
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_CUBE_MEASURES";                              
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_FACTTBLKEYMAPS";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRFACTTBLKEYMAPS";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_FACTTBLFCTMAPS";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRFACTTBLFCTMAPS";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_HIERDIM_KEYCOL_MAP";                         
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_HIER_CUSTOM_SORT";                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_OLAP2_AGG_USES";                             
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_CWM1_AGGOP";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_CWM1_AGGORD";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_ENTITY_PARAMETERS";                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$MRALL_ENTITY_EXT_PARMS";                           
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AWS_T";                                      
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_MAP_DIM_USE_T";                           
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_PHYS_OBJ_T";                              
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_PHYS_OBJ_PROP_T";                         
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_MAP_MEAS_USE_T";                          
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_MAP_ATTR_USE_T";                          
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_DIMENSIONS_T";                            
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_ATTRIBUTES_T";                            
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBES_T";                                 
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_DIM_USES_T";                         
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_DIM_LEVELS_T";                            
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_MEASURES_T";                         
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_AGG_SPECS_T";                        
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_AGG_MEAS_T";                         
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_AGG_LVL_T";                          
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_CUBE_AGG_OP_T";                           
@pdc4_if_null2.sql "OLAPSYS" "MRAC_OLAP2_AW_HIER_LVL_ORD_T";                          
@pdc4_if_null2.sql "FLOWS_FILES" "WWV_FLOW_FILE_OBJECTS$";                            
@pdc4_if_null2.sql "OWBSYS" "OWBRTPS";                                                
@pdc4_if_null2.sql "SCOTT" "DEPT";                                                    
@pdc4_if_null2.sql "SCOTT" "EMP";                                                     
@pdc4_if_null2.sql "SCOTT" "BONUS";                                                   
@pdc4_if_null2.sql "SCOTT" "SALGRADE";                                                
@pdc4_if_null2.sql "DBSNMP" "AQ_MNTR_MSGS_QUEUE";                                     
@pdc4_if_null2.sql "DBSNMP" "AQ_MNTR_MSGS_SUBS";                                      
@pdc4_if_null2.sql "DBSNMP" "AQ_MNTR_MSGS_PERSQ";                                     
@pdc4_if_null2.sql "DBSNMP" "AQ_MNTR_MSGS_PERSQSUBS";                                 
@pdc4_if_null2.sql "DBSNMP" "AQ_MNTR_MSGS_BUFFQ";                                     
@pdc4_if_null2.sql "RQL" "R";                                                         
@pdc4_if_null2.sql "RQL" "FOO";                                                       
@pdc4_if_null2.sql "RQL" "BAR";                                                       
@pdc4_if_null2.sql "RQL" "DESCRIPTIONS";                                              
@pdc4_if_null2.sql "RQL" "CONTENTS";                                                  
@pdc4_if_null2.sql "RQL" "ASSOCIATIONS";                                              
@pdc4_if_null2.sql "RQL" "MIN_SAMPLES";                                               
@pdc4_if_null2.sql "RQL" "TEMPERATURE_PER_MINUTE_COPY";                               
@pdc4_if_null2.sql "RQL" "DESC_INT";                                                  
@pdc4_if_null2.sql "RQL" "TEMP_IDS";                                                  
@pdc4_if_null2.sql "RQL" "SAMPLES_PER_HOUR_TEMPERATURE";                              
@pdc4_if_null2.sql "RQL" "TEMPERATURE_PER_HOUR";                                      
@pdc4_if_null2.sql "RQL" "TEMPERATURE_PER_MINUTE";                                    
@pdc4_if_null2.sql "RQL" "MINUTE_ALL_SENSORS";                                        
@pdc4_if_null2.sql "RQL" "SENSORS";                                                   
@pdc4_if_null2.sql "RQL" "SENSOR_NAMES";                                              
@pdc4_if_null2.sql "RQL" "SENSOR_DESCRIPTIONS";                                       
@pdc4_if_null2.sql "AMESMOUDI" "OBJECT";                                              
@pdc4_if_null2.sql "AMESMOUDI" "SOURCE";                                              
@pdc4_if_null2.sql "RQL" "SAMPLES_EVERY_MINUTE";                                      
@pdc4_if_null2.sql "RQL" "SAMPLES_AGGREGATED_PER_MINUTE";                             
@pdc4_if_null2.sql "SOUHIR" "PRINCIPALE";                                             
@pdc4_if_null2.sql "SOUHIR" "PERSONNE";                                               
@pdc4_if_null2.sql "AZHAR" "EMP_WM_LT";                                               
@pdc4_if_null2.sql "AZHAR" "EMP_WM_VT";                                               
@pdc4_if_null2.sql "AZHAR" "EMP_WM_AUX";                                              
@pdc4_if_null2.sql "AZHAR" "PAYMENT";                                                 
@pdc4_if_null2.sql "AZHAR" "STEP";                                                    
@pdc4_if_null2.sql "AZHAR" "PROCESS";                                                 
@pdc4_if_null2.sql "AZHAR" "PREVIOUS_STEP_REL_LT";                                    
@pdc4_if_null2.sql "AZHAR" "PRIORITY";                                                
@pdc4_if_null2.sql "AZHAR" "ACCOUNT";                                                 
@pdc4_if_null2.sql "AZHAR" "CURRENCY";                                                
@pdc4_if_null2.sql "AZHAR" "CUTOFF";                                                  
@pdc4_if_null2.sql "AZHAR" "CLEARING_HOUSE";                                          
@pdc4_if_null2.sql "PLAMARRE" "GEO_MOUNTAIN";                                         
@pdc4_if_null2.sql "SOUHIR" "R100N10S10";                                             
@pdc4_if_null2.sql "SOUHIR" "R100N10S50";                                             
@pdc4_if_null2.sql "SOUHIR" "R100MN10S10";                                            
@pdc4_if_null2.sql "AZHAR" "RHYTHM_1_MIN";                                            
@pdc4_if_null2.sql "AZHAR" "RHYTHM_1_DAY";                                            
@pdc4_if_null2.sql "PLAMARRE" "COMMUNES";                                             
@pdc4_if_null2.sql "PLAMARRE" "DEPARTEMENTS";                                         
@pdc4_if_null2.sql "PLAMARRE" "ETATS";                                                
@pdc4_if_null2.sql "PLAMARRE" "PAYS";                                                 
@pdc4_if_null2.sql "PLAMARRE" "REGIONS";                                              
@pdc4_if_null2.sql "AZHAR" "PREVIOUS_STEP_REL_VT";                                    
@pdc4_if_null2.sql "AZHAR" "PREVIOUS_STEP_REL_AUX";                                   
@pdc4_if_null2.sql "SOUHIR" "R100MN10S50";                                            
@pdc4_if_null2.sql "SOUHIR" "R30M";                                                   
@pdc4_if_null2.sql "SOUHIR" "R40M";                                                   
@pdc4_if_null2.sql "SOUHIR" "R20M";                                                   
@pdc4_if_null2.sql "SOUHIR" "R100MN10S1";                                             
@pdc4_if_null2.sql "SOUHIR" "ORIGINALE";                                              
@pdc4_if_null2.sql "SOUHIR" "R100MN10S5";                                             
@pdc4_if_null2.sql "SOUHIR" "R100M";                                                  
@pdc4_if_null2.sql "SOUHIR" "R50MINDEX";                                              
@pdc4_if_null2.sql "SOUHIR" "R75M";                                                   
@pdc4_if_null2.sql "SOUHIR" "NUMERIQUE";                                              
@pdc4_if_null2.sql "SOUHIR" "NUM30";                                                  
@pdc4_if_null2.sql "SOUHIR" "NUM50";                                                  
@pdc4_if_null2.sql "SOUHIR" "NUM10";                                                  
@pdc4_if_null2.sql "SOUHIR" "JN1000NUM";                                              
@pdc4_if_null2.sql "SOUHIR" "JN1000NUMINDEX";                                         
@pdc4_if_null2.sql "SOUHIR" "JN1000CH";                                               
@pdc4_if_null2.sql "SOUHIR" "JN1000CHINDEX";                                          
@pdc4_if_null2.sql "SOUHIR" "JN1000CHINDEX1";                                         
@pdc4_if_null2.sql "SOUHIR" "JN1000NUM1";                                             
@pdc4_if_null2.sql "SOUHIR" "JN1000NUMINDEX1";                                        
@pdc4_if_null2.sql "SOUHIR" "JN1000CH1";                                              
@pdc4_if_null2.sql "GLOBAL" "OBJECT";                                                 
@pdc4_if_null2.sql "GLOBAL" "OBJECT2";                                                
@pdc4_if_null2.sql "GLOBAL" "SOURCE";                                                 
@pdc4_if_null2.sql "GLOBAL" "SOURCE3";                                                
@pdc4_if_null2.sql "GLOBAL" "OBJECT3";                                                
@pdc4_if_null2.sql "GLOBAL" "REFSRCMATCH";                                            
@pdc4_if_null2.sql "GLOBAL" "SCIENCE_CCD_EXPOSURE_METADATA";                          
@pdc4_if_null2.sql "AZHAR" "A_LT";                                                    
@pdc4_if_null2.sql "AZHAR" "B_LT";                                                    
@pdc4_if_null2.sql "AZHAR" "B_VT";                                                    
@pdc4_if_null2.sql "AZHAR" "B_AUX";                                                   
@pdc4_if_null2.sql "AZHAR" "A_VT";                                                    
@pdc4_if_null2.sql "AZHAR" "A_AUX";                                                   
@pdc4_if_null2.sql "AZHAR" "U2_EMP_M_LT";                                             
@pdc4_if_null2.sql "AZHAR" "U2_EMP_LT";                                               
@pdc4_if_null2.sql "AZHAR" "U2_EMP_NUM_LT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SEX_LT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SAL_LT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_PHONE_LT";                                         
@pdc4_if_null2.sql "AZHAR" "U2_EMP_NUM_VT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_NUM_AUX";                                          
@pdc4_if_null2.sql "AZHAR" "U2_EMP_PHONE_VT";                                         
@pdc4_if_null2.sql "AZHAR" "U2_EMP_PHONE_AUX";                                        
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SEX_VT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SEX_AUX";                                          
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SAL_VT";                                           
@pdc4_if_null2.sql "AZHAR" "U2_EMP_SAL_AUX";                                          
@pdc4_if_null2.sql "AZHAR" "U2_EMP_VT";                                               
@pdc4_if_null2.sql "AZHAR" "U2_EMP_AUX";                                              
@pdc4_if_null2.sql "AZHAR" "U2_EMP_M_VT";                                             
@pdc4_if_null2.sql "AZHAR" "U2_EMP_M_AUX";                                            
@pdc4_if_null2.sql "GLOBAL" "CHANNEL_DIM";                                            
@pdc4_if_null2.sql "GLOBAL" "CUSTOMER_DIM";                                           
@pdc4_if_null2.sql "GLOBAL" "PRICE_AND_COST_HIST_FACT";                               
@pdc4_if_null2.sql "GLOBAL" "PRICE_AND_COST_UPD_FACT";                                
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_CLASS_DSC";                                      
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_CLASS_MEMBER";                                   
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_DIM";                                            
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_FAMILY_DSC";                                     
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_FAMILY_MEMBER";                                  
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_ITEM_BUYER";                                     
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_ITEM_DSC";                                       
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_ITEM_MARKETING_MANAGER";                         
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_ITEM_MEMBER";                                    
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_ITEM_PACKAGE";                                   
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_TOTAL_PRODUCT_DSC";                              
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_TOTAL_PRODUCT_MEMBER";                           
@pdc4_if_null2.sql "GLOBAL" "TIME_DIM";                                               
@pdc4_if_null2.sql "GLOBAL" "TIME_MONTH_DIM";                                         
@pdc4_if_null2.sql "GLOBAL" "TIME_YEAR_DIM";                                          
@pdc4_if_null2.sql "GLOBAL" "TIME_QUARTER_DIM";                                       
@pdc4_if_null2.sql "GLOBAL" "UNITS_HISTORY_FACT";                                     
@pdc4_if_null2.sql "GLOBAL" "UNITS_UPDATE_FACT";                                      
@pdc4_if_null2.sql "GLOBAL" "PRODUCT_CHILD_PARENT";                                   
@pdc4_if_null2.sql "GLOBAL" "FORECAST_METHOD_DIM";                                    
@pdc4_if_null2.sql "GLOBAL_AW" "CUBE_BUILD_LOG";                                      
@pdc4_if_null2.sql "GLOBAL_AW" "CUBE_OPERATIONS_LOG";                                 
@pdc4_if_null2.sql "GLOBAL_AW" "CUBE_REJECTED_RECORDS";                               
@pdc4_if_null2.sql "GLOBAL_AW" "CUBE_DIMENSION_COMPILE";                              
@pdc4_if_null2.sql "MANEL" "TESTSEM";                                                 
@pdc4_if_null2.sql "FABIEN" "R";                                                      
@pdc4_if_null2.sql "MANEL" "RTEST4";                                                  
@pdc4_if_null2.sql "MANEL" "TAB1";                                                    
@pdc4_if_null2.sql "MANEL" "TAB2";                                                    
@pdc4_if_null2.sql "MANEL" "TAB3";                                                    
@pdc4_if_null2.sql "MANEL" "R";                                                       
@pdc4_if_null2.sql "MANEL" "R1";                                                      
@pdc4_if_null2.sql "MANEL" "CLASSROOM";                                               
@pdc4_if_null2.sql "MANEL" "DEPARTMENT";                                              
@pdc4_if_null2.sql "MANEL" "COURSE";                                                  
@pdc4_if_null2.sql "MANEL" "INSTRUCTOR";                                              
@pdc4_if_null2.sql "MANEL" "SECTION";                                                 
@pdc4_if_null2.sql "MANEL" "TEACHES";                                                 
@pdc4_if_null2.sql "MANEL" "STUDENT";                                                 
@pdc4_if_null2.sql "MANEL" "TAKES";                                                   
@pdc4_if_null2.sql "MANEL" "ADVISOR";                                                 
@pdc4_if_null2.sql "MANEL" "PREREQ";                                                  
@pdc4_if_null2.sql "GLOBAL" "EXPORT_JOB_SQLDEV_2933";                                 
@pdc4_if_null2.sql "GLOBAL_AW" "DBMS_CUBE_EXP$$CLOB_TBL";                             
@pdc4_if_null2.sql "M2TI00" "CUBE_BUILD_LOG";                                         
@pdc4_if_null2.sql "M2TI00" "CUBE_DIMENSION_COMPILE";                                 
@pdc4_if_null2.sql "M2TI00" "CUBE_OPERATIONS_LOG";                                    
@pdc4_if_null2.sql "GLOBAL" "ETUDIANTS";                                              
@pdc4_if_null2.sql "M2TI00" "CUBE_REJECTED_RECORDS";                                  
@pdc4_if_null2.sql "M2TI00" "DBMS_CUBE_EXP$$CLOB_TBL";                                
@pdc4_if_null2.sql "MANEL" "RTEST";                                                   
@pdc4_if_null2.sql "GLOBAL" "REGIONS_TEST";                                           
@pdc4_if_null2.sql "MANEL" "RTEST2";                                                  
@pdc4_if_null2.sql "MANEL" "RTEST3";                                                  
@pdc4_if_null2.sql "MANEL" "RTESTTEXT";                                               
@pdc4_if_null2.sql "MANEL" "RTEST5";                                                  
@pdc4_if_null2.sql "FABIEN" "PLOP";                                                   
@pdc4_if_null2.sql "FABIEN" "BEGIN";                                                  
@pdc4_if_null2.sql "FABIEN" "EXOPL";                                                  
@pdc4_if_null2.sql "DBSNMP" "MGMT_TEMPT_SQL";                                         
@pdc4_if_null2.sql "DBSNMP" "BSLN_TIMEGROUPS";                                        
@pdc4_if_null2.sql "DBSNMP" "MGMT_DB_FILE_GTT";                                       
@pdc4_if_null2.sql "DBSNMP" "MGMT_DB_SIZE_GTT";                                       
@pdc4_if_null2.sql "OLAPSYS" "CWM2$OLAPMANAGERTABLE";                                 
@pdc4_if_null2.sql "OLAPSYS" "CWM2$_AW_NEXT_TEMP_CUST_MEAS";                          
@pdc4_if_null2.sql "OLAPSYS" "CWM2$_AW_TEMP_CUST_MEAS_MAP";                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$_TEMP_VALUES";                                     
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWDIMCREATEACCESS";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$AWCUBECREATEACCESS";                               
@pdc4_if_null2.sql "OLAPSYS" "CWM2$OLAPVALIDATETABLE";                                
@pdc4_if_null2.sql "OLAPSYS" "CWM2$OLAPEXPORTCOMMANDTABLE";                           
@pdc4_if_null2.sql "OLAPSYS" "CWM2$OLAPEXPORTOBJECTTABLE";                            
@pdc4_if_null2.sql "OLAPSYS" "OLAP_SESSION_OBJECTS";                                  
@pdc4_if_null2.sql "OLAPSYS" "OLAP_SESSION_DIMS";                                     
@pdc4_if_null2.sql "OLAPSYS" "OLAP_SESSION_CUBES";                                    
@pdc4_if_null2.sql "GLOBAL_AW" "AW$GLOBAL";                                           
@pdc4_if_null2.sql "M2TI00" "AW$GLOBAL";                                              
@pdc4_if_null2.sql "HDUSER" "SOURCE";                                                 
@pdc4_if_null2.sql "RQL" "SAMPLES";                                                   
@pdc4_if_null2.sql "HDUSER" "OBJECT";                                                 

