/*script qui génère une requete permettant de générer 
la requete mysql pour peupler les valeurs nulles */

/* Rem: pour mettre un ' dans un fichier il faut noter || ' '' ' ||
        2 quotes entre quote  = 1 quote */

/*On définit 2 'variables' qui seront le nom d'utilisateur et le nom de la table*/
define aUSER_NAME = &1;
define aTABLE_NAME= &2;
        
SET TERMOUT off;
SET verify off;
SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool tempValuesNull/&aUSER_NAME..&aTABLE_NAME..sql;

select 
'select ' || '''' || 'update TABLES set nbNullValues=' 
|| '''' || ' || '''''''' || ' || '(' || count(*) ||'*count(*)' 	/*calcul de toutes les valeurs*/
from DBA_TAB_COLUMNS
where OWNER='&aUSER_NAME.' and TABLE_NAME='&aTABLE_NAME.';
 
/*le calcul des valeurs non nulls*/
select '-count(' ||COLUMN_NAME||')' 
from DBA_TAB_COLUMNS where OWNER='&aUSER_NAME.' and TABLE_NAME='&aTABLE_NAME.'
/*certaines colonnes de types large Objects (grosses données variables) ne sont pas acceptés par count */
and DATA_TYPE not in ('BLOB', 'CLOB', 'LONG');

select ') ' || 
/*le where de la requete d'insertion dans la base mysql*/
' || '''''''' || ' || '''' || /* =====correspond à: || '''' || ' */
' where name='||''''|| ' || '''''''' || ' || '''' || '&aTABLE_NAME.'||''''||
' || '''''''' || ' || '''' || 
' AND user_id IN (SELECT id FROM USERS WHERE name='||''''||
' || '''''''' || ' || ''''|| '&aUSER_NAME.'||''''|| ' || '''''''' || '
|| '''' || ') AND database_id=2;' || '''' ||
' from ' || OWNER || '.' || TABLE_NAME ||
';' 
from DBA_TABLES 
where OWNER='&aUSER_NAME.' and TABLE_NAME='&aTABLE_NAME.';

spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;
