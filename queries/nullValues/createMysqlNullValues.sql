SET TERMOUT off;
SET verify off;
SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
SPOOL antePopulateNullValues.sql;

/*Les requetes qui servent que l'écriture doivent renvoyer qu'une seule ligne*/
/*Requete qui sert à rien pour écrire le DEBUT du fichier*/
select
'SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
SPOOL temp/LIRISnullValues.sql;'
from DBA_USERS where USERNAME='SYS';

/*Requete qui appelle chaque fichier auto générée préalablement*/
select '@tempValuesNull/' || OWNER || '.' || TABLE_NAME || '.sql;'
from DBA_TABLES
where OWNER NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');

/*Requete qui sert à rien pour écrire le FIN du fichier*/
select
'SPOOL OFF;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;' 
from DBA_USERS where USERNAME='SYS';

SPOOL off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;