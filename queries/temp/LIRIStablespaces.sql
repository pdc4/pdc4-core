INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'SYSTEM', 'PERMANENT', 'ONLINE');                                                                  
INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'SYSAUX', 'PERMANENT', 'ONLINE');                                                                  
INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'UNDOTBS1', 'UNDO', 'ONLINE');                                                                     
INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'TEMP', 'TEMPORARY', 'ONLINE');                                                                    
INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'USERS', 'PERMANENT', 'ONLINE');                                                                   
INSERT INTO TABLESPACES (DATABASE_ID, NAME, TYPE, STATUS) VALUES ('2', 'PETASKY_LIRIS', 'PERMANENT', 'ONLINE');                                                           

