Description :
Donne le nom de la table, son propriétaire et le nombre d'attribut par table

La deuxieme requete donne le nombre moyen d'attribut par table par utilisateur

la troisième requete donne le nombre moyen d'attribut par table

La quatrième peuple la base avec les tables, les owner et le nombre de colonnes 

Query :
SELECT OWNER,TABLE_NAME,COUNT(*) 
FROM ALL_TAB_COLUMNS 
GROUP BY TABLE_NAME,OWNER 
ORDER BY COUNT(*) DESC

SELECT OWNER,AVG(ATTRIBUTE_NUMBER)AS AVG_ATTRIBUTE_USER FROM(
SELECT OWNER,TABLE_NAME, COUNT(*) AS ATTRIBUTE_NUMBER
FROM ALL_TAB_COLUMNS 
GROUP BY TABLE_NAME,OWNER 
ORDER BY COUNT(*) DESC ) 
GROUP BY OWNER
ORDER BY AVG_ATTRIBUTE_USER DESC


SELECT AVG(ATTRIBUTE_NUMBER)AS AVG_ATTRIBUTE FROM(
SELECT OWNER,TABLE_NAME, COUNT(*) AS ATTRIBUTE_NUMBER
FROM ALL_TAB_COLUMNS 
GROUP BY TABLE_NAME,OWNER 
ORDER BY COUNT(*) DESC ) 
ORDER BY AVG_ATTRIBUTE DESC


SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool E:\tables.sql;
select 'INSERT Tables ('|| 'OWNER' ||', ' || 'NAME' ||', ' || 'NbAttributes' ||') SELECT ' || ' id  ,'''  || TABLE_NAME ||''', ''' || COUNT(*) ||''' FROM USERS WHERE name='''  || OWNER || ''';'
from  ALL_TAB_COLUMNS 
GROUP BY TABLE_NAME,OWNER ;
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;