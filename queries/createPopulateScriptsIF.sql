# creer le script utilisateurs 
SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFusers.sql;
select 'INSERT INTO USERS ('|| 'NAME' ||', ' || 'STATUS' ||', ' || 'INSTANCE_ID' ||') VALUES (''' || USERNAME ||''', ''' || ACCOUNT_STATUS ||''', ''' || '1' ||''');'
from dba_users
WHERE username  NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;

# creer le script tables 

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFtables.sql;
select 'INSERT TABLES (database_id, '|| 'user_id' ||', ' || 'NAME' ||',' || 'nbAttributes' ||') SELECT 1, ' || ' (SELECT id FROM USERS WHERE instance_id=1 AND name =''' || OWNER ||''')  ,'''  || TABLE_NAME ||''', ''' || COUNT(*) ||''' FROM USERS WHERE name='''  || OWNER || ''';'
from  DBA_TAB_COLUMNS
GROUP BY TABLE_NAME,owner
HAVING OWNER  NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB') AND OWNER IN (SELECT OWNER FROM DBA_TABLES) AND TABLE_NAME IN (SELECT TABLE_NAME FROM DBA_TABLES WHERE DBA_TABLES.OWNER = DBA_TAB_COLUMNS.OWNER)  ;
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;

#CREER LE SCRIPT NBValues 

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFnbVal.sql;
SELECT '
SET HEADING off;
SET PAGESIZE 0;*
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFvals\' || OWNER || '.' || TABLE_NAME || '.sql 
  '
  
  ||'SELECT ''UPDATE TABLES SET NbValues= ''' ||  '|| COUNT(*)||' || ''' WHERE name=''||''''''''||'''||TABLE_NAME||'''||''''''''||' ||
  ''' AND user_id IN'||'(SELECT id FROM USERS' ||   ' WHERE name=''||''''''''||''' || OWNER || '''||' || '''''''' ||'''||'') AND database_id=1;'''|| ' FROM ' || OWNER ||'.' || TABLE_NAME 
  ||';\n
  spool off;
  SET HEADING on;
  SET ECHO on;
  SET FEEDBACK on;'
FROM dba_tables
WHERE OWNER NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;
@C:\Users\Utlisateur\pdc4-core\queries\temp\IFnbVal.sql;


-- insert tablespaces

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFtablespaces.sql;
select 'INSERT INTO TABLESPACES ('|| 'DATABASE_ID' ||', ' || 'NAME' ||', ' || 'TYPE' ||', ' || 'STATUS' ||') VALUES (''' || '1' ||''', ''' || TABLESPACE_NAME ||''', ''' || CONTENTS ||''', ''' || STATUS ||''');'
from dba_TABLESPACES;
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;

-- insert datafiles 

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFtablespacesFiles.sql;
select 'UPDATE TABLESPACES SET '|| 'fileName=''' || FILE_NAME ||''', ' || 'BYTES=' || BYTES ||', ' || 'BLOCKS=' || BLOCKS ||' WHERE database_id=1 AND NAME=''' || TABLESPACE_NAME ||''';'
from dba_data_files;
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;


--INSERT TEMP DATAFILEs


SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFtablespacesTempFiles.sql;
select 'UPDATE TABLESPACES SET '|| 'fileName=''' || FILE_NAME ||''', ' || 'BYTES=' || BYTES ||', ' || 'BLOCKS=' || BLOCKS ||' WHERE database_id=1 AND NAME=''' || TABLESPACE_NAME ||''';'
from dba_temp_files;
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;

-- insert segments

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFsegments.sql;
select 'INSERT SEGMENTS ('||'name'|| ',' || 'user_id' ||', '|| 'tablespace_id' ||',' || 'type' ||',' || 'subtype' ||',' || 'bytes' ||',' || 'blocks' ||') SELECT ''' || SEGMENT_NAME || ''',' || ' (SELECT id FROM USERS WHERE instance_id=1 AND name =''' || OWNER ||''')  ,'  || ' (SELECT id FROM TABLESPACES WHERE database_id=1 AND name =''' || TABLESPACE_NAME ||''')  ,'''  || SEGMENT_TYPE ||''', ''' || SEGMENT_SUBTYPE ||''', ''' || BYTES ||''', ''' || BLOCKS ||''' FROM USERS WHERE name='''  || OWNER || ''';'
from  DBA_SEGMENTS
WHERE OWNER  NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;

--insert view number

SET HEADING off;
SET PAGESIZE 0;
SET ECHO off;
SET FEEDBACK off;
spool C:\Users\Utlisateur\pdc4-core\queries\temp\IFviewCount.sql;
select 'UPDATE homestead.DATABASES SET '|| 'nbViews=' || COUNT(*)  || ' WHERE id=1 ' ||';'
FROM DBA_VIEWS
WHERE OWNER NOT IN ('SYS', 'SYSTEM', 'MDSYS', 'CTXSYS','SYSMAN', 'EXFSYS','APEX_030200','ORDDATA','WMSYS','XDB');
spool off;
SET HEADING on;
SET ECHO on;
SET FEEDBACK on;
