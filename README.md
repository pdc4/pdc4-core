# README #

## Dossiers ##

### Homestead ###
Ce dossier contient les fichiers nécessaires à la VM commune mise en place avec Vagrant.

### documents ###
Ce dossier contient la configuration nécessaire à nginx (pour la VM). 

### front ###
Ce dossier contient le code source de l'application web.

### queries ###
Ce dossier contient les scripts d'extraction des données.

### server ###
Ce dossier contient l'application PHP fournissant l'API pour l'application web sur la base de données.